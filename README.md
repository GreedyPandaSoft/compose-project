# Docker Compose Project



## Créer un projet sous Docker

1) Créez un dossier pour votre projet.

2) Créez et copiez le contenu du fichier « Dockerfile » :
```
FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/
```

3) Créez et copiez le contenu du fichier « requirements.txt »:
```
Django>=3.0,<4.0
psycopg2-binary>=2.8
```

4) Créez et copiez le contenu du fichier « docker-compose.yml »:
```
version: "3.9"
services:
    db:
        image: postgres
        environment:
            - POSTGRES_DB=postgres
            - POSTGRES_USER=postgres
            - POSTGRES_PASSWORD=postgres
    web:
        build: .
        command: python manage.py runserver 0.0.0.0:8000
        volumes:
        - .:/code
        ports:
        - "8000:8000"
        depends_on:
        - db
```

## Utiliser Docker-Compose

1) A la racine du projet, lancez la configuration avec docker-compose run

```
sudo docker-compose run web django-admin startproject composeexample .
```

2) Changez les droits d’accès au dossier avec la commande suivate :
```
sudo chown -R $USER:$USER .
```

3) Ajoutez en haut du fichier composeexample/settings.py la ligne:
```
import os
```
4) Modifiez la permission d'accès dans le fichier composeexample/settings.py:
```
ALLOWED_HOSTS = ['*']
```

5) Editez la partie DATABASE du fichier composeexample/settings.py :
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('POSTGRES_NAME'),
        'USER': os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': 'db',
        'PORT': 5432,
    }
}
```

6) Démarrez le projet avec docker-compose :
```
sudo docker-compose up
```

7) Accéder à Django: localhost:8000
